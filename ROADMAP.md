# Roadmap

Where is this going? Who knows.

Some ideas:

+ Fiction as EDN files.
+ Support for setting and reading variables.
+ Support for stats.
+ Support for constrained choices --
  choices that only appear if a given condition obtains.
+ Random generation.
